// Fill out your copyright notice in the Description page of Project Settings.


#include "LowSpeedBonus.h"
#include  "SnakeBase.h"

// Sets default values
ALowSpeedBonus::ALowSpeedBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALowSpeedBonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALowSpeedBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALowSpeedBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if(bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if(IsValid(Snake))
		{
			Snake -> LowSpeed();
			GEngine-> AddOnScreenDebugMessage(-1, 15.0f,FColor::Green,TEXT("Low speed bonus"));
			this->Destroy();
		}
	}
}


